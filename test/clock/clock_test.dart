import 'package:chess_clock/clock/state.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("New instance", () {
    test("it starts with ten minutes for white", () {
      var state = ClockState();
      expect(state.white.time, Duration(minutes: 10, seconds: 0));
    });
    test("it starts with ten minutes for black", () {
      var state = ClockState();
      expect(state.black.time, Duration(minutes: 10, seconds: 0));
    });
    test("it starts with ten minutes for current player", () {
      var state = ClockState();
      expect(state.currentPlayer.time, Duration(minutes: 10, seconds: 0));
    });
    test("It starts with current player as black", () {
      var state = ClockState();
      expect(state.black, state.currentPlayer);
    });
    test("It begins paused", () {
      var state = ClockState();
      expect(state.paused, true);
    });
  });
  group("move", () {
    test("It is not paused after moving", () {
      var state = ClockState();
      state.move();
      expect(state.paused, false);
    });
    test("Whites clock is running after the first move", () {
      var state = ClockState();
      state.move();
      expect(state.white.clock.isRunning, true);
    });
    test("Blacks clock is not running after the first move", () {
      var state = ClockState();
      state.move();
      expect(state.black.clock.isRunning, false);
    });
    test("It is whites turn after the first move", () {
      var state = ClockState();
      state.move();
      expect(state.currentPlayer, state.white);
    });
    test("It is blacks turn after the second move", () {
      var state = ClockState();
      state.move();
      state.move();
      expect(state.currentPlayer, state.black);
    });
    test("Blacks clock is running after the second move", () {
      var state = ClockState();
      state.move();
      state.move();
      expect(state.black.clock.isRunning, true);
    });
    test("Whites clock is not running after the second move", () {
      var state = ClockState();
      state.move();
      state.move();
      expect(state.white.clock.isRunning, false);
    });
  });
  group('pause', () {
    test('It should pause the clock', () {
      var state = ClockState();
      state.move();
      expect(state.paused, false);
    });
    test('It should pause a game', () {
      var state = ClockState();
      state.move();
      state.pause();
      expect(state.paused, true);
    });
    test('It should stop the current Players, clock', () {
      var state = ClockState();
      state.move();
      state.pause();
      expect(state.currentPlayer.clock.isRunning, false);
    });
  });
  group('resume', () {
    test(
        'It should not start the current player clock if the game hasnt started yet',
        () {
      var state = ClockState();
      state.resume();
      expect(state.currentPlayer.clock.isRunning, false);
    });
    test('It should not start the clock if the game has not started', () {
      var state = ClockState();
      state.resume();
      expect(state.currentPlayer.clock.isRunning, false);
    });
    test('it should unpause the game', () {
      var state = ClockState();
      state.move();
      state.pause();
      state.resume();
      expect(state.paused, false);
    });
    test('it should start the current players clock', () {
      var state = ClockState();
      state.move();
      state.pause();
      state.resume();
      expect(state.currentPlayer.clock.isRunning, true);
    });
  });
  group('turn', () {
    test('It should be black to start the clock', () {
      var state = ClockState();
      expect(state.turn, ClockState.BLACK);
    });
    test('It should be whites turn after the first move', () {
      var state = ClockState();
      state.move();
      expect(state.turn, ClockState.WHITE);
    });
  });
}
