import 'package:chess_clock/clock/logic.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'clock/view.dart';

void main() => runApp(
      GetMaterialApp(initialRoute: 'clock', getPages: [
        GetPage(
            name: 'clock',
            page: () => Clock(),
            binding: BindingsBuilder(
              () => {Get.lazyPut<ClockLogic>(() => ClockLogic())},
            )),
      ]),
    );
