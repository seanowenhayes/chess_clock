class Player {
  var clock = Stopwatch();
  var time = Duration(minutes: 1, seconds: 0);
  Duration timeLeft() => time - clock.elapsed;
  bool timeOver() => clock.elapsed > time;

  void reset() {
    clock = Stopwatch();
    time = Duration(minutes: 1, seconds: 0);
  }
}

class ClockState {
  bool _started = false;
  bool _gameOver = false;

  bool get isGameOver {
    return _gameOver;
  }

  late Player black;
  Player white = Player();
  late Player currentPlayer;
  var paused = true;

  static const BLACK = "BLACK";
  static const WHITE = "WHITE";

  String turn = BLACK;

  ClockState() {
    Player player = Player();
    black = player;
    currentPlayer = player;
  }

  bool get _gameIsActive {
    return _started && !_gameIsActive;
  }

  void reset() {
    _started = false;
    _gameOver = false;
    turn = BLACK;
    black.reset();
    white.reset();
    currentPlayer = black;
  }

  move() {
    if (!_gameOver) {
      _started = true;
      paused = false;
      currentPlayer.clock.stop();
      if (currentPlayer == white) {
        currentPlayer = black;
        turn = BLACK;
      } else {
        currentPlayer = white;
        turn = WHITE;
      }
      currentPlayer.clock.start();
    }
  }

  pause() {
    if (_gameIsActive) {
      paused = true;
      currentPlayer.clock.stop();
    }
  }

  resume() {
    if (_gameIsActive) {
      paused = false;
      currentPlayer.clock.start();
    }
  }

  gameOver() {
    _gameOver = true;
    currentPlayer.clock.stop();
  }
}
