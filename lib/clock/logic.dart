import 'dart:async';

import 'package:get/get.dart';

import 'state.dart';

class ClockLogic extends GetxController {
  final state = ClockState();
  late Timer _timer;

  ClockLogic() : super() {
    _timer = Timer.periodic(Duration(milliseconds: 10), tick);
  }

  void tick(Timer timer) {
    if (state.currentPlayer.timeOver() && !state.isGameOver) {
      state.gameOver();
    }
    update();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void pause() {
    state.pause();
    update();
  }

  void resume() {
    state.resume();
    update();
  }

  void move() {
    state.move();
    update();
  }

  void reset() {
    state.reset();
    update();
  }
}
