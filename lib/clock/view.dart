import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class Clock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GetBuilder<ClockLogic>(
        builder: (controller) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (controller.state.isGameOver)
                AlertDialog(
                  content:
                      Text('Game Over ${controller.state.turn} Lost on time'),
                  actions: [
                    TextButton(
                      onPressed: () {
                        controller.reset();
                      },
                      child: Text('New Game'),
                    ),
                  ],
                ),
              Text(controller.state.turn),
              Expanded(
                child: TextButton(
                  child: Text('WHITE ${controller.state.white.timeLeft()}'),
                  onPressed: controller.state.turn == ClockState.WHITE
                      ? () {
                          print('Whites turn...');
                          controller.move();
                        }
                      : null,
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                ),
              ),
              Expanded(
                child: TextButton(
                  onPressed: controller.state.turn == ClockState.BLACK
                      ? () {
                          print('Black pressed');
                          controller.move();
                        }
                      : null,
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  child: Text('BLACK ${controller.state.black.timeLeft()}'),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
